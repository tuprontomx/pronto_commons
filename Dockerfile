FROM python:3.9

LABEL mantainer="Pronto Team <dev@tupronto.mx>"


RUN mkdir -p /opt/commons

WORKDIR /opt/commons

ADD requirements.txt requirements.txt
ADD test-requirements.txt test-requirements.txt

RUN pip install -r test-requirements.txt
RUN pip install -r requirements.txt