import pytest


@pytest.fixture(autouse=True)
def set_mongoengine():
    from mongoengine import connect, get_db

    connect("mongoenginetest", host="mongomock://localhost")
    yield
    get_db().client.drop_database("mongoenginetest")
