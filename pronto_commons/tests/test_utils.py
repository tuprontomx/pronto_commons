from pronto_commons.utils import (
    can_change_matrix,
    generate_pagination,
    get_jwt_token,
    get_url_path,
    remove_whitespaces_from_string,
)


def test_get_jwt_token_empty():
    assert get_jwt_token(dictionary={}) == {}


def test_get_jwt_token():
    assert get_jwt_token(dictionary={"jwt_token": {"1": "1"}}) == {"1": "1"}


def test_get_url_path():

    assert get_url_path(url="https://hola.com/adios") == "/adios"
    assert get_url_path(url="https://hola.com/") == "/"
    assert get_url_path(url="https://hola.com") == ""


def test_generate_pagination():

    pagination = generate_pagination(page=1, total_items=5, items_per_page=1)

    assert pagination["has_previous"] is False
    assert pagination["has_more"] is True
    assert pagination["page"] == 1
    assert pagination["total_items"] == 5
    assert pagination["total_pages"] == 5

    pagination = generate_pagination(page=2, total_items=5, items_per_page=1)

    assert pagination["has_previous"] is True
    assert pagination["has_more"] is True
    assert pagination["page"] == 2
    assert pagination["total_items"] == 5
    assert pagination["total_pages"] == 5

    pagination = generate_pagination(page=5, total_items=5, items_per_page=1)

    assert pagination["has_previous"] is True
    assert pagination["has_more"] is False
    assert pagination["page"] == 5
    assert pagination["total_items"] == 5
    assert pagination["total_pages"] == 5

    pagination = generate_pagination(page=1, total_items=1, items_per_page=1)

    assert pagination["has_previous"] is False
    assert pagination["has_more"] is False
    assert pagination["page"] == 1
    assert pagination["total_items"] == 1
    assert pagination["total_pages"] == 1


def test_can_change_matrix():

    assert can_change_matrix(old_value=1, new_value=1, matrix={1: [2]}) is True
    assert can_change_matrix(old_value=1, new_value=3, matrix={1: [2]}) is False
    assert can_change_matrix(old_value=1, new_value=2, matrix={1: [2]}) is True


def test_remove_whitespaces_from_string():
    assert (
        remove_whitespaces_from_string(
            string="""Victor
    Castillo"""
        )
        == "Victor Castillo"
    )

    assert (
        remove_whitespaces_from_string(string="""Victor    Castillo""")
        == "Victor Castillo"
    )

    assert remove_whitespaces_from_string(string=" Hello Testing ") == "Hello Testing"
