import pytest
from bson import ObjectId
from bson.errors import InvalidId
from mongoengine import Document, StringField
from pronto_commons.mongo import MongoContext, get_jwt_id, transform_location_to_geojson


class TestModelMongo(Document):
    name = StringField(required=True)


def test_get_jwt_id():

    _dict = {"jwt_token": {"_id": str(ObjectId())}}

    assert isinstance(get_jwt_id(dictionary=_dict), ObjectId)


def test_get_jwt_id_exception():
    with pytest.raises(InvalidId):
        get_jwt_id(dictionary={})


def test_transform_location_to_geojson():
    lat = 10
    lon = 20
    geojson = transform_location_to_geojson(latitude=lat, longitude=lon)

    assert geojson["coordinates"] == [float(lon), float(lat)]
    assert geojson["type"] == "Point"


def test_MongoContext():
    with MongoContext(TestModelMongo, {"name": "Name", "save_before_deleting": True}):
        assert TestModelMongo.objects.count() == 1
    assert TestModelMongo.objects.count() == 0
