import datetime

from pronto_commons.dates import (
    date_to_datetime,
    datetime_with_timezone,
    get_iso_week_day,
    local_date_to_utc,
)

timezone = "America/Mexico_City"


def test_get_iso_week_day_mid_day():
    date = datetime.datetime(2021, 1, 1, 12, 0, 0)
    weekday = get_iso_week_day(date=date, timezone=timezone)

    assert weekday == 5  # Friday


def test_get_iso_week_day_one_am():
    date = datetime.datetime(2021, 1, 1, 1, 0, 0)  # UTC this is thursday in Mexico time
    weekday = get_iso_week_day(date=date, timezone=timezone)

    assert weekday == 4  # Thursday


def test_datetime_with_timezone():
    date = datetime.datetime(2021, 1, 1, 1, 0, 0)  # UTC this is thursday in Mexico time
    datetime_tz = datetime_with_timezone(datetime=date, timezone=timezone)
    assert datetime_tz.tzinfo is not None
    assert datetime_tz.tzinfo.zone == timezone


def test_date_to_datetime():
    date = datetime.date(2021, 1, 1)
    new_datetime = date_to_datetime(date=date)

    assert new_datetime == datetime.datetime(2021, 1, 1, 0, 0, 0)


def test_local_date_to_utc():
    date = datetime.date(2021, 1, 1)
    new_datetime = local_date_to_utc(date=date, timezone=timezone)
    new_datetime = new_datetime.replace(
        tzinfo=None
    )  # We remove the tzinfo, just tomake the comparison easier

    assert new_datetime == datetime.datetime(2021, 1, 1, 6, 0, 0)
