from unittest.mock import patch

from pronto_commons.exceptions import BusinessException
from pronto_commons.sentry import log_exception


class ExceptionError(BusinessException):
    pass


def test_log_exception():
    with patch("pronto_commons.sentry.capture_exception") as capture_exception:
        log_exception(exception=KeyError())
        log_exception(exception=ExceptionError())
        log_exception(exception=ExceptionError(), force_capture_business_exception=True)
        assert capture_exception.call_count == 2
