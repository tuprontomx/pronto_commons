from pronto_commons.exceptions import BusinessException


def test_BusinessException_serialize():
    extra_info = {"something": "something"}
    exc = BusinessException(extra_info=extra_info)

    assert exc.serialize() == {
        "extra_info": extra_info,
        "code": BusinessException.ERROR_CODE,
        "message": BusinessException.ERROR_MESSAGE,
    }
