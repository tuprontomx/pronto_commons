from pronto_commons.env import (
    EnvName,
    is_development,
    is_production,
    is_staging,
    is_testing,
)


def test_is_development():

    assert is_development(environment="development") is True
    assert is_development(environment="something esle") is False


def test_is_production():

    assert is_production(environment="production") is True
    assert is_production(environment="something esle") is False


def test_is_staging():

    assert is_staging(environment="staging") is True
    assert is_staging(environment="something esle") is False


def test_is_testing():

    assert is_testing(environment="testing") is True
    assert is_testing(environment="something esle") is False
