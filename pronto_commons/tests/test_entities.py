from pronto_commons.entities import PhoneNumber


def test_PhoneNumber_internanional_format():
    phone = PhoneNumber(country_code=52, national_number=1234567890)
    assert phone.international_format == "+521234567890"
