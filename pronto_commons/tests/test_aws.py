import boto3
from moto import mock_s3
from pronto_commons.aws import upload_file_to_s3


@mock_s3
def test_upload_file_to_s3():
    access_key = "dummy"
    secret_key = "dummy"
    bucket_name = "dummy"
    key = "dummy"
    file = b""

    conn = boto3.resource("s3", region_name="us-east-1")
    # We need to create the bucket since this is all in Moto's 'virtual' AWS account
    conn.create_bucket(Bucket=bucket_name)

    s3_file = upload_file_to_s3(
        access_key=access_key,
        secret_key=secret_key,
        bucket_name=bucket_name,
        key=key,
        file=file,
    )

    assert s3_file.key == key
