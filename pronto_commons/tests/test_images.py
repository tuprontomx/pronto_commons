import io

from PIL import Image
from pronto_commons.images import (
    compress_image,
    convert_base64_to_image,
    return_extension_from_base64,
)


def test_return_extension_from_base64_none():
    assert return_extension_from_base64(base64_string="") is None


def test_return_extension_from_base64():
    image = """data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
        9TXL0Y4OHwAAAABJRU5ErkJggg=="""
    assert return_extension_from_base64(base64_string=image) == ".png"


def test_return_extension_from_base64_jpg():
    image = """data:image/jpeg;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
        9TXL0Y4OHwAAAABJRU5ErkJggg=="""
    assert return_extension_from_base64(base64_string=image) == ".jpg"


def test_convert_base64_to_image():
    image = """data:image/jpeg;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
        9TXL0Y4OHwAAAABJRU5ErkJggg=="""
    bytes_object = convert_base64_to_image(base64_string=image)

    assert isinstance(bytes_object, io.BytesIO)
    assert bytes_object.getbuffer().nbytes > 0


def test_compress_image_already_compressed():
    image = """data:image/jpeg;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
        9TXL0Y4OHwAAAABJRU5ErkJggg=="""
    bytes_object = convert_base64_to_image(base64_string=image)

    copy_object = convert_base64_to_image(base64_string=image)

    new_bytes = compress_image(image_bytes=bytes_object, extension="jpg")

    assert new_bytes.getbuffer().nbytes == copy_object.getbuffer().nbytes


def test_compress_image_already_not_compressed_image_too_small():
    image = """data:image/png;base64, iVBORw0KGgoAAAANSUhEUgAAAAUA
    AAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO
        9TXL0Y4OHwAAAABJRU5ErkJggg=="""
    bytes_object = convert_base64_to_image(base64_string=image)

    copy_object = convert_base64_to_image(base64_string=image)

    new_bytes = compress_image(
        image_bytes=bytes_object, extension="png", maximum_image_size_bytes=80
    )

    assert new_bytes.getbuffer().nbytes == copy_object.getbuffer().nbytes


def test_compress_image_already_not_compressed_image_file():
    image = Image.open("pronto_commons/tests/testing_image.png", mode="r")
    bytes_object = io.BytesIO()
    image.save(bytes_object, format="PNG")

    total_bytes = bytes_object.getbuffer().nbytes

    bytes_object.seek(0)

    new_bytes = compress_image(
        image_bytes=bytes_object,
        extension="png",
        maximum_image_size_bytes=total_bytes * 0.80,
    )
    bytes_object.seek(0)

    assert new_bytes.getbuffer().nbytes <= bytes_object.getbuffer().nbytes
